package com.ms.models;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "registro")
public class Registro implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Id
    private long id;
    @Column(name = "nombre_cliente")
    private String nombreCliente;
    @Column(name = "apellido_cliente")
    private String apellidoCliente;
    @Column(name = "telefono_cliente")
    private String telefonoCliente;
    @Column(name = "fecha_llamada")
    private String fechaLlamada;
    @Column(name = "tipo_servicio")
    private String tipoServicio;
    @Column(name = "servicio_solicitado")
    private String servicioSolicitado;
    @Column(name = "observaciones")
    private String observaciones;
    @Column(name = "estado")
    private String estado;
    @Column(name = "creado_por")
    private String creadoPor;
    @Column(name = "modificado_por")
    private String modificadoPor;
    @Column(name = "fecha_creacion")
    private String fechaCreacion;
    @Column(name = "fecha_modificacion")
    private String fechaModificacion;

    public Registro() {
    }

    public Registro(long id, String nombreCliente, String apellidoCliente, String telefonoCliente, String fechaLlamada, String tipoServicio, String servicioSolicitado, String observaciones, String estado, String creadoPor, String modificadoPor, String fechaCreacion, String fechaModificacion) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.telefonoCliente = telefonoCliente;
        this.fechaLlamada = fechaLlamada;
        this.tipoServicio = tipoServicio;
        this.servicioSolicitado = servicioSolicitado;
        this.observaciones = observaciones;
        this.estado = estado;
        this.creadoPor = creadoPor;
        this.modificadoPor = modificadoPor;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getFechaLlamada() {
        return fechaLlamada;
    }

    public void setFechaLlamada(String fechaLlamada) {
        this.fechaLlamada = fechaLlamada;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getServicioSolicitado() {
        return servicioSolicitado;
    }

    public void setServicioSolicitado(String servicioSolicitado) {
        this.servicioSolicitado = servicioSolicitado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCreadoPor() {
        return creadoPor;
    }

    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
