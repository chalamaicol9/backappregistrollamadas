package com.ms.controller;

import com.ms.models.Usuario;
import com.ms.service.UsuarioService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/api")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/listUsuario")
    public List<Usuario> listUsuario() {
        return usuarioService.findAll();
    }

    @PostMapping("/crearUsuario")
    public ResponseEntity<?> createCliente(@RequestBody Usuario usuario, BindingResult result) {
        Usuario newCliente = null;
        Map<String, Object> response = new HashMap<>();

        if (usuarioService.existsByCorreo(usuario.getEmail())) {
            response.put("mensaje", "Error al Crear el cliente");
            response.put("error", "El correo electrónico ya existe");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream().
                    map(err -> "el campo '" + err.getField() + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);

        }
        try {

            usuario.setFechaCreacion(new Date());
            usuario.setFechaModificacion(new Date());
            usuario.setEstado(Boolean.TRUE);
            String encriptarPassword = passwordEncoder.encode(usuario.getPassword());
            usuario.setPassword(encriptarPassword);
            newCliente = usuarioService.createUsuario(usuario);
        } catch (DataAccessException e) {
            response.put("mensaje", "error al Crear el usuario");
            response.put("error", e.getMessage().concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
        response.put("mensaje", "El usuario ha sido creado con éxito!");
        response.put("usuario", newCliente);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
    
    @PutMapping("/activarInactivarUsuario/{id}/{estado}")
    public ResponseEntity<?> ActivarIncativaUsuario(@RequestBody Usuario usuario, @PathVariable Integer id,@PathVariable Boolean estado, BindingResult result) {

        Usuario usuarioActual = usuarioService.findById(id);

        Map<String, Object> response = new HashMap<>();
        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if (usuarioActual == null) {
            response.put("mensaje", "Error: no se pudo editar, el usuario ID: "
                    .concat(id.toString().concat(" no existe en la base de datos!")));
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        try {
            
            usuarioActual.setEstado(!estado);
            Usuario usuarioActualizado = usuarioService.createUsuario(usuarioActual);
            if (usuarioActualizado.isEstado() == true) {
                response.put("mensaje", "Se a activado el usuario!");
                response.put("Usuario", usuarioActualizado);
            } else {
                response.put("mensaje", "Se a inactivado EL usuario");
                response.put("Usuario", usuarioActualizado);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al cambiar el estado del usuario");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
