package com.ms.dao;

import com.ms.models.Usuario;
import java.util.List;

public interface UsuarioDao {
    
   public List<Usuario> findAll();
   
  public Usuario createUsuario(Usuario usuario);
   
   public boolean existsByCorreo(String correo);
   
   public Usuario findById(Integer id);
}
