package com.ms.security;

import com.ms.models.Usuario;
import com.ms.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;


@Service
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    private  UsuarioRepository usuariosRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        System.out.println("UserDetailServiceImpl");
            Usuario usuario = usuariosRepository
                    .findOneByEmail(email)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        
        return new UserDetailsImpl(usuario);

    }
}
